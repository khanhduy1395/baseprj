import Api from './Api';

const login = (email, password) => {

  const parameters = {
      email: email,
      password: password
  }
    return Api.get(`/login`, {params:parameters});
  
};

// export
const api = { login };
export default api;