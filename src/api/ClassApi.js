import Api from "./Api";

const url = "/classes";

const getAll = () => {
  return Api.get(`${url}`);
};

const api = { getAll };
export default api;
