import Api from "./Api";

const url = "/users";

const existsByEmail = (email) => {
  return Api.get(`${url}/email/${email}`);
};

const existsByUsername = (username) => {
  return Api.get(`${url}/userName/${username}`);
};

const create = (username, email, password, firstname, lastname, phone, address, description) => {
  const body = {
    userName: username,
    email: email,
    password: password,
    firstName: firstname,
    lastName: lastname,
    phone: phone,
    address: address,
    description: description,
  };
  return Api.post(url, body);
};

const resendEmailToActiveAccount = (email) => {
  const requestParams = {
    email: email,
  };

  return Api.get(`${url}/userRegistrationConfirmRequest`, { params: requestParams });
};

const getProfile = () => {
  return Api.get(`${url}/profile`);
};

const updateProfile = (avatarUrl) => {
  const body = {
      avatarUrl: avatarUrl
  }
  return Api.put(`${url}/profile`, body);
};

const update = (id, phone, address, description) => {
  const body = {
    phone,
    address,
    description,
  };
  return Api.put(`${url}/${id}`, body);
};

const getById = (id) => {
  return Api.get(`${url}/${id}`);
};
// export

const api = { updateProfile, existsByEmail, existsByUsername, create, resendEmailToActiveAccount, getProfile, getById, update };
export default api;
