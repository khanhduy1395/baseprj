import Api from "./Api";

const url = "/requests";

const getAll = () => {
  return Api.get(`${url}`);
};


const create = (classId,userId,className) => {
  const body = {
    classId,
    userId,
    className
  }
  return Api.post(url, body);
}
// export
const api = { getAll,create };
export default api;