import React from "react";
import aduy1 from "../../assets/img/avatars/aduy1.jpg"
import aduy2 from "../../assets/img/avatars/aduy2.jpg"
import aquyet from "../../assets/img/avatars/aquyet.jpg"
import atuan from "../../assets/img/avatars/atuan.jpg"
import m7 from "../../assets/img/avatars/m7.jpg"
import chimy from "../../assets/img/avatars/chimy.jpg"
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  CardImg,
  CardTitle,
  Col,
  Container,
  DropdownToggle,
  ListGroup,
  ListGroupItem,
  Progress,
  Row,
  UncontrolledDropdown
} from "reactstrap";

import { MoreHorizontal } from "react-feather";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";


const Project = ({ name, state, color, percentage, description, image }) => (
  <Card>
    {image ? <CardImg top src={image} alt="Card image cap" style={{height: "350px"}}/> : ""}
    <CardHeader className="px-4 pt-4">
      <div className="card-actions float-right">
        <UncontrolledDropdown>
          <DropdownToggle tag="a">
            <MoreHorizontal />
          </DropdownToggle>
        </UncontrolledDropdown>
      </div>
      <CardTitle tag="h2" className="mb-0">
        {name}
      </CardTitle>
      <Badge className="my-2" color={color}>
        {state}
      </Badge>
    </CardHeader>
    <CardBody className="px-4 pt-2">
      <p>{description}</p>
    </CardBody>
    <ListGroup flush>
      <ListGroupItem className="px-4 pb-4">
        <p className="mb-2 font-weight-bold">
         
Experience
          <span className="float-right">{percentage}%</span>
        </p>
        <Progress className="progress-sm" value={percentage} />
      </ListGroupItem>
    </ListGroup>
  </Card>
);

const Projects = () => (
  <Container fluid className="p-0">
    <Button color="primary" className="float-right mt-n1">
      <FontAwesomeIcon icon={faPlus} /> New Mentor
    </Button>
    <h1 className="h3 mb-3">ABOUT US </h1>
    <h3>The Mentor Team</h3>
    <Row>
      <Col md="6" lg="3">
        <Project
          name="Nguyễn Quyết"
          state="CEO VTI Academy"
          color="success"
          percentage="100"
          description=" CEO Giám đốc VTI Academy ."
          image={aquyet}
        />
      </Col>
      <Col md="6" lg="3">
        <Project
          name="Nguyễn Ngọc Duy"
          state="Mentor"
          color="success"
          percentage="90"
          description="Mentor Fullstack ."
          image={aduy1}
        />
      </Col>
      <Col md="6" lg="3">
        <Project
          name="Nguyễn Duy Tuấn"
          state="Mentor"
          color="success"
          percentage="80"
          description="Mentor Mock Project."
          image={atuan}
        />
      </Col>
      
      <Col md="6" lg="3">
        <Project
          name="Phạm Khánh Duy"
          state="Mentor"
          color="success"
          percentage="80"
          description="Mentor Reactjs"
          image={aduy2}
        />
      </Col>
      
    </Row>
    <Row className="pt-1 ml-2"> 
      <h2 className="text-center">
      Excellent student
      </h2>
    </Row>
    <Row className="pl-7 mt-2">
    <Col md="5" lg="5">
        <Project
          name="Nguyễn Đức Mạnh(REACTJS)"
          state="Front-end"
          color="danger"
          percentage="60"
          description="."
          image={m7}
        />
      </Col>
      <Col md="5" lg="5">
        <Project
          name="Neo Nguyen(JAVA SPRING)"
          state="Back-end"
          color="danger"
          percentage="60"
          description="."
          image={chimy}
        />
      </Col>
    </Row>
  </Container>
);

export default Projects;
