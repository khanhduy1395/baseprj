import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { toastr } from "react-redux-toastr";
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Container,
  FormGroup,
  Label,
  Input,
  Row,
  CustomInput,
} from "reactstrap";
// import avatar1 from "../../assets/img/avatars/avatar.jpg";
import { Briefcase, Home, MapPin, MessageSquare } from "react-feather";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGlobe } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faInstagram,
  faLinkedin,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
//API
import UserApi from "../../api/UserApi";

const ProfileDetails = (props) => {
  const [userInfo, setUserInfo] = useState([]);
  console.log(props);
  useEffect(() => {
    const getProfile = async () => {
      const result = await UserApi.getProfile();
      console.log(result);
      // update storage,redux .....
      setUserInfo(result);
    };
    getProfile();
  }, []);
  useEffect(() => {
    if (props.data && Object.keys(props?.data).length !== 0) {
      const newInfo = {
        ...userInfo,
        address: props.data.title,
        phone: props.data.message,
        description: props.data.type === "success" ? "Reacjs" : props.data.type,
      };
      console.log("newInfo", newInfo);
      setUserInfo(newInfo);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data]);

  return (
    <Card>
      <CardHeader>
        <CardTitle tag="h5" className="mb-0">
          Profile Details
        </CardTitle>
      </CardHeader>
      <CardBody className="text-center">
        <img
          src={
            "https://scontent.fhan3-1.fna.fbcdn.net/v/t1.6435-9/244713377_1823656164505026_7253674031425945091_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=ElSrQ3YdYEgAX8M15jS&_nc_ht=scontent.fhan3-1.fna&oh=36c80873e719b3c88d1748b391806de3&oe=61A2B4FD"
          }
          alt={userInfo.firstName + " " + userInfo.lastName}
          className="img-fluid rounded-circle mb-2"
          width="128"
          height="128"
        />
        <CardTitle tag="h5" className="mb-0">
          {userInfo.firstName + " " + userInfo.lastName}
        </CardTitle>
        <div className="text-muted mb-2">{userInfo.role}</div>

        <div>
          <Button size="sm" color="primary" className="mr-1">
            Follow
          </Button>
          <Button size="sm" color="primary">
            <MessageSquare width={16} height={16} /> Message
          </Button>
        </div>
      </CardBody>

      <hr className="my-0" />

      <CardBody>
        <CardTitle tag="h5">Skills</CardTitle>
        <Badge color="primary" className="mr-1 my-1">
          HTML
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          JavaScript
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          Sass
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          Angular
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          Vue
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          React
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          Redux
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          UI
        </Badge>
        <Badge color="primary" className="mr-1 my-1">
          UX
        </Badge>
      </CardBody>

      <hr className="my-0" />
      <CardBody>
        <CardTitle tag="h5">About</CardTitle>
        <ul className="list-unstyled mb-0">
          <li className="mb-1">
            <Home width={14} height={14} className="mr-1" /> Lives in{" "}
            <Link to="/dashboard/default">{userInfo.address}</Link>
          </li>

          <li className="mb-1">
            <Briefcase width={14} height={14} className="mr-1" /> Phone{" "}
            <Link to="/dashboard/default">{userInfo.phone}</Link>
          </li>
          <li className="mb-1">
            <MapPin width={14} height={14} className="mr-1" /> Work{" "}
            <Link to="/dashboard/default">{userInfo.description}</Link>
          </li>
        </ul>
      </CardBody>
      <hr className="my-0" />
      <CardBody>
        <CardTitle tag="h5">Elsewhere</CardTitle>

        <ul className="list-unstyled mb-0">
          <li className="mb-1">
            <FontAwesomeIcon icon={faGlobe} fixedWidth className="mr-1" />
            <Link to="/dashboard/default">staciehall.co</Link>
          </li>
          <li className="mb-1">
            <FontAwesomeIcon icon={faTwitter} fixedWidth className="mr-1" />
            <Link to="/dashboard/default">Twitter</Link>
          </li>
          <li className="mb-1">
            <FontAwesomeIcon icon={faFacebook} fixedWidth className="mr-1" />
            <Link to="/dashboard/default">Facebook</Link>
          </li>
          <li className="mb-1">
            <FontAwesomeIcon icon={faInstagram} fixedWidth className="mr-1" />
            <Link to="/dashboard/default">Instagram</Link>
          </li>
          <li className="mb-1">
            <FontAwesomeIcon icon={faLinkedin} fixedWidth className="mr-1" />
            <Link to="/dashboard/default">LinkedIn</Link>
          </li>
        </ul>
      </CardBody>
    </Card>
  );
};

class Activities extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      message: "",
      type: "success",
      timeOut: 5000,
      showCloseButton: true,
      progressBar: true,
      // position: "top-right"
    };
  }

  showToastr() {
    const options = {
      timeOut: parseInt(this.state.timeOut),
      showCloseButton: this.state.showCloseButton,
      progressBar: this.state.progressBar,
      // position: this.state.position
    };

    const toastrInstance =
      this.state.type === "reactjs"
        ? toastr.info
        : this.state.type === "java"
        ? toastr.warning
        : this.state.type === "sql"
        ? toastr.error
        : toastr.success;

    toastrInstance("Update Success!", options);
  }

  render() {
    return (
      <Container fluid className="p-0">
        <h1 className="h3 mb-3">Update Profile</h1>

        <Card>
          <CardHeader>
            <CardTitle tag="h5">Change Infomation</CardTitle>
          </CardHeader>
          <CardBody>
            <Row>
              <Col md="9">
                <FormGroup>
                  <Label for="type">Description</Label>
                  <CustomInput
                    id="type"
                    type="select"
                    value={this.state.type}
                    onChange={(e) => this.setState({ type: e.target.value })}
                  >
                    <option value="reactjs">REACTJS</option>
                    <option value="java">JAVA</option>
                    <option value="sql">SQL</option>
                    <option value="spring">SPRING</option>
                  </CustomInput>
                </FormGroup>

                <FormGroup>
                  <Label for="title">Address</Label>
                  <Input
                    type="text"
                    value={this.state.title}
                    onChange={(e) => this.setState({ title: e.target.value })}
                    placeholder="Enter a title"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="message">Phone Number</Label>
                  <Input
                    type="text"
                    value={this.state.message}
                    onChange={(e) => this.setState({ message: e.target.value })}
                    placeholder="Enter a message"
                  />
                </FormGroup>

                <FormGroup>
                  <Label for="timeOut">Duration</Label>
                  <CustomInput
                    id="duration"
                    type="select"
                    value={this.state.timeOut}
                    onChange={(e) => {
                      this.setState({ timeOut: e.target.value });
                    }}
                  >
                    <option value={2500}>2.5s</option>
                    <option value={5000}>5s</option>
                    <option value={7500}>7.5s</option>
                    <option value={10000}>10s</option>
                  </CustomInput>
                </FormGroup>
              </Col>
            </Row>

            <hr />

            <Button
              onClick={() => {
                this.showToastr();
                console.log("state", this.state);
                // eslint-disable-next-line no-undef
                this.props.handleChange(this.state);
              }}
              type="button"
              className="mr-1"
              color="primary"
            >
              Update
            </Button>
          </CardBody>
        </Card>
      </Container>
    );
  }
}

const Profile = () => {
  const [data, setData] = useState({});
  const infoChange = (info) => {
    console.log("info", info);
    setData(info);
  };
  return (
    <Container fluid className="p-0">
      <h1 className="h3 mb-3">Profile</h1>

      <Row>
        <Col md="4" xl="3">
          <ProfileDetails data={data} />
        </Col>
        <Col md="8" xl="9" pt="0">
          <Activities handleChange={infoChange} />
        </Col>
      </Row>
    </Container>
  );
};

export default Profile;
