import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { enableCorporateTheme } from "../../redux/actions/themeActions";

import {
  Badge,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Collapse,
  Container,
  Media,
  Nav,
  NavItem,
  NavLink,
  Navbar,
  NavbarBrand,
  Row,
} from "reactstrap";

import {
  Box,
  Chrome,
  Code,
  DownloadCloud,
  Mail,
  Sliders,
  Smartphone,
} from "react-feather";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

// import screenshotDashboardDefault from "../../assets/img/screenshots/dashboard-default.jpg";
// import screenshotDashboardAnalytics from "../../assets/img/screenshots/dashboard-analytics.jpg";
// import screenshotDashboardEcommerce from "../../assets/img/screenshots/dashboard-e-commerce.jpg";
// import screenshotDashboardSocial from "../../assets/img/screenshots/dashboard-social.jpg";
// import screenshotDashboardCrypto from "../../assets/img/screenshots/dashboard-crypto.jpg";
// import screenshotPageProjects from "../../assets/img/screenshots/pages-projects-list.jpg";

// import screenshotThemeCorporate from "../../assets/img/screenshots/theme-corporate.jpg";
// import screenshotThemeModern from "../../assets/img/screenshots/theme-modern.jpg";
// import screenshotThemeClassic from "../../assets/img/screenshots/theme-classic.jpg";

import brandBootstrap from "../../assets/img/brands/bootstrap.svg";
import brandBootstrapB from "../../assets/img/brands/b.svg";
import brandSass from "../../assets/img/brands/sass.svg";
import brandWebpack from "../../assets/img/brands/webpack.svg";
import brandNpm from "../../assets/img/brands/npm.svg";
import brandReact from "../../assets/img/brands/react.svg";
import brandRedux from "../../assets/img/brands/redux.svg";
import react from "../../assets/img/avatars/react.png";
// import phucloi from "../../assets/img/avatars/phucloi.jpg";
// import kinhcong from "../../assets/img/avatars/kinhcong.jpg";
// import vtii from "../../assets/img/avatars/vtii.png";
import no1 from "../../assets/img/avatars/js.png";
import no2 from "../../assets/img/avatars/00.png";
import no3 from "../../assets/img/avatars/1.png";
import no4 from "../../assets/img/avatars/5.jpg";
import no5 from "../../assets/img/avatars/6.jpg";
import no6 from "../../assets/img/avatars/7.jpg";

const Navigation = () => (
  <Navbar dark expand="md" className="navbar-landing">
    <Container>
      <NavbarBrand href="/">
        <Box title="Nguyen Duc Manh" />
        Nguyen Duc Manh
      </NavbarBrand>
      <Nav className="ml-auto" navbar>
        <NavItem className="d-none d-md-inline-block">
          <NavLink href="/dashboard/default" target="_blank" active>
            Preview
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="/docs/introduction" target="_blank" active>
            Documentation
          </NavLink>
        </NavItem>
        <NavItem className="d-none d-md-inline-block">
          <NavLink href="mailto:support@bootlab.io" active>
            Support
          </NavLink>
        </NavItem>
      </Nav>
      <Button
        href="https://www.facebook.com/nguyen.d.manh.714/"
        target="_blank"
        rel="noopener noreferrer"
        color="primary"
        className="ml-2"
        size="lg"
      >
        View Info
      </Button>
    </Container>
  </Navbar>
);

const Intro = () => {
  const handleClick = (e) => {
    document.getElementById("demos").scrollIntoView();
    e.preventDefault();
  };

  return (
    <section className="landing-intro pt-5">
      <Container
        style={{
          backgroundImage: `url("https://www.google.com/url?sa=i&url=https%3A%2F%2Ftoidicodedao.com%2F2018%2F08%2F21%2Fseries-luoc-su-lap-trinh-web-phan-4-1-reactjs-ngang-troi-xuat-the%2F&psig=AOvVaw2nqbGLckw-9PxzGc_fIext&ust=1623211602344000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKDj99OWh_ECFQAAAAAdAAAAABAJ")`,
        }}
      >
        <Row>
          <Col md="7" className="mx-auto text-center">
            <h1 className="landing-intro-title my-4">
              Nguyen Duc Manh's university graduation thesis
            </h1>

            <p className="landing-intro-subtitle">
              A professional package that comes with plenty of UI components,
              forms, tables, charts, dashboards, pages and svg icons. Each one
              is fully customizable, responsive and easy to use.
            </p>

            <div className="my-4">
              <a
                href="#demos"
                onClick={handleClick}
                className="btn btn-light btn-lg mr-1"
              >
                About Me
              </a>{" "}
              <a
                href="/auth/sign-in"
                target="_blank"
                rel="noopener noreferrer"
                className="btn btn-outline-light btn-lg"
              >
                HomePage
              </a>
            </div>

            <div className="my-4">
              <img
                src={brandBootstrap}
                alt="Bootstrap"
                width="40"
                className="align-middle mr-2"
              />
              <img
                src={brandSass}
                alt="Sass"
                width="40"
                className="align-middle mr-2"
              />
              <img
                src={brandWebpack}
                alt="Webpack"
                width="40"
                className="align-middle mr-2"
              />
              <img
                src={brandNpm}
                alt="NPM"
                width="40"
                className="align-middle mr-2"
              />
              <img
                src={brandReact}
                alt="React"
                width="40"
                className="align-middle mr-2"
              />
              <img
                src={brandRedux}
                alt="Redux"
                width="36"
                className="align-middle mr-2"
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col md="8" className="mx-auto text-center">
            <div className="mt-4 landing-intro-img">
              <img
                src={react}
                className="img-fluid rounded-lg"
                alt="Corporate Bootstrap 4 Dashboard Theme"
              />
            </div>
          </Col>
        </Row>
      </Container>

      <svg
        className="landing-intro-shape"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 220"
      >
        <path
          fill="#F7F9FC"
          fill-opacity="1"
          d="M0,160L1440,32L1440,320L0,320Z"
        ></path>
      </svg>
    </section>
  );
};

const Styles = () => (
  <section id="demos" className="pt-3 pb-6">
    <Container className="position-relative z-3">
      <Row>
        <Col md="12" className="mx-auto text-center">
          <Row>
            <Col lg="10" xl="9" className="mx-auto">
              <div className="mb-4">
                <h2 className="mb-3">Academic level</h2>
                <p className="text-muted font-size-lg">
                  Nguyen Duc Manh - 17113155 - PM22.12- Đại học Kinh Doanh Và
                  Công Nghệ Hà Nội
                </p>
              </div>
            </Col>
          </Row>

          <Row>
            <Col md="4" className="py-3">
              <Link to="#" target="_blank" className="d-block mb-3 mx-1">
                <img
                  src={
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQeduDE2Pz1ODNeujWUsvlSMp8bm2pziOWHg&usqp=CAU"
                  }
                  alt="Corporate Bootstrap 4 React Dashboard Theme"
                  className="img-fluid rounded-lg landing-img academic"
                />
              </Link>
              <h4>
                Junior{" "}
                <sup>
                  <Badge color="primary" tag="small">
                    New
                  </Badge>
                </sup>
              </h4>
            </Col>

            <Col md="4" className="py-3">
              <Link to="#" target="_blank" className="d-block mb-3 mx-1">
                <img
                  src={
                    "https://static.wixstatic.com/media/8870af_5881839203e74d44b5c9dfaa3f5ad7e1~mv2.png/v1/fit/w_600%2Ch_350%2Cal_c/file.png"
                  }
                  alt="Modern Bootstrap 4 React Dashboard Theme"
                  className="img-fluid rounded-lg landing-img academic"
                />
              </Link>
              <h4>
                Fresher{" "}
                <sup>
                  <Badge color="primary" tag="small">
                    New
                  </Badge>
                </sup>
              </h4>
            </Col>

            <Col md="4" className="py-3">
              <Link to="#" target="_blank" className="d-block mb-3 mx-1">
                <img
                  src={
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABCFBMVEX///8zMzL307o8PDsAAAAjHyDt7/CoqKn8xJ339/gYGBX/2sAmJiYlJSP71r0TExEcFxja2toQCQv9wZeio6MeHRsIAAAsLCvy9PXLzc4jKSwwLC3m5uYbJCj0vpkYExSriHC4t7fYqYmMi4tJSUjR0dEtMTKRkJCzsrLe3t4rMDHpyLGrlYXsqo7y4tjGxsYADBZWVFRmZGRAQD+ZmZhUT0ueinxlXFbVt6KDdGpwZV3Bp5USHyWSgHR7bmQAFR7uspf0ya9xcXB/fX7cvafIrJkLGRteZGX+8ezKkXikl41MR0PNt6iadmb+6t2ypp7r0sLEl4IAABTwwKvqooL5zKzqtpJVRz4C57F+AAALL0lEQVR4nO2dC1fiSBbHE1IE0wGMEQhCGJblKQ+RgCgitoLuOmt3r9u2M9//m2y98uJtYCYJp37n2JJAKvdfdeveWwkdOY7BYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8Fg/CXout8W/NU0QJqfpqqOPVnfbNk74qwkcRwvCDwfT08bItwFt7nYwG/D9ob+RUtPz6uagOC1tNGQOKPEdcHBSDxPQGGJaU+gQJGzUsI419Il+G7Y5ydyST2BdQkOeE3gK4KQ1jmx77eJO5Ioc30pLqyiwjWOq5tbCTBSmu8Jld5KhVpsps38NnIniIOuAXZA2m8jd2KjQkg81G4qpTcr5FN+W7kT/BYKp1w3tPVNqmtsVihoHB9aR60ebyFQ0ErHXb8t9YY+qGwjUBD6WslvW71xEte2UyhoIR1Djhv0t1MYP/fbUq+IW4RSRFry21KPSIktFVb8ttQr1e7AIXFOrXNT64XWTadIx1Uu16zVmsPXnKWqVrgdNWvNZq52hcTWz0Kb8lHRdi3LrZvxcCTLN6NcE4qs5frDltwatcdPEbmVgwrLftvpGby6n8iRSDIpo3/l5HhUex3e0I1kMhKR0biGdxVcggmxNoY6LKBSWXbukG+hxPAuoGJQYe7JKWgBeQgVJkJ7sQYrXCswkhw3w6wQeemVvF4hCjWJsGZ87kwT+NF6hRG5GeZ5qCeEWnu9l0bk6xDXNOgiRvMGJgWcGeaVJsk+ecLDVX5o4YVcRH4at4dtmN4daQImjeTTzfPzTUuWxzU+xJf3Db7/3C8U7jCF0Y1MeRpe0X394bgW1gUwoszDGu12/NRqwcJtlCs0r0eTyei6Wbi7nrTH43H7HpasQpgVNjQh9yyj6YYn4zAn8BAhN4HeieehHLnmQ7wAhusnzZUt5HaNzzWF3Njel4zUwrsA5lC6cGcLmb9vPV3dOlMkLEx5v83chX9hhcgdccUt37Zl+JOkFXgSK6z9228rd+Dl99y1HEk+/6dhvL5Ov0Xkq3FSvm/LrW+z19fX0rCFaprXB7/N3IGHzvc7qOLpx8t/R5OLH63IZSuSbEMvffk2mlRf3uDW893vnTe/7fTOQ6eTu5dpjkeRBtXhrUua8/E0vBI6YVb43um0v7bNWkZufb1BddrtvRVq5OHX/3U6F37b6Z23TudxcnlLaplW++uQSMvdtkht0xpdDn/r/Hrx207vXHQ67/Lz1eVd/7VfuLx6lmkOvP35ej+Z3F995cfyQ+eX32buwMuvzjuccrBkG49vInblLbfGw8lo0r6BGaPTefDbzF2A5jtWSq61k5wkCbHTaflt5S68dx42rIAfQx1o4ET81VkvECn028jduOg8rlf48BDiupvw5howqPfBpTnEmcLkx9ygvbsH1W/z9sFaJ/3ht3V7YY3AEFekLg5eICetEJj027D98XLoApdLPCiBtsTH9wMVuDiKBydwXuLhCcyKou5ME6IY+nLUBdQHcQkUD0mjJBLeTo+OPj4+jo5OL+gevy3bE6ZAUYQKMR/WnoMYxqwlR7ygEnXxkCTaI4gkfpyenkace8Iv0SVwKX5buCsbBYZeorQZv030RIo/JnzZBvrZSoi+zz79wntAAw2/Dd+Wk2MvAiHHflu+LY2ER4VfwvKljJhnhSd+m74lTCFTGHy8R5qwKEwdusLsoSvMeleYDoXCrLhOoRaPa2sUnvlt/RbA9eAahWkjlTLSaxQGfymFFryrFca7jem00Y2vVpgNvERxncJ0d5rWtPS0u2oUkcKASxTXKuyV8KrqS6m3TmGg/yuiuFahNivjKKOVZyuiDVYYZInZTQrreAbG6+sVBvfym3lhdKWXClW8ND6uCuu8NLgSreuGKxXGB7HjROI4NlgVTE2FwYw29oXRi/ZKBfVqLFatr0yIlsJASjTlRY5O/5FbJSGeMIzEynTIF/6ZDO49GyrvFN2ZWK0Qhps1RRtUeHR6FMEiAxdQbXkbFK4FKoQQkQGTmBXfTHm7K8QiL4IVUCUxYuvbh0J8E9VvVU6sG4P7U3h0Gig/FT+O9q/wLUCDKImn+1d4FAmQQs6h8DfIDgrh0WZLj0FSmDW99A/+5+XlZcGjQJ6/u7z8qf1BvTRQwfSNDuKfhXh87ZWYDaCjC38ShQF7ksQHkYgFxleXZdsojBfIEP4NVpteYt2Jnr817dx+RBPw6DuR6HUQicDvaDK/Lb0ZvmyHd4FnAODbeSWAOOG4HnDCcWXw98JxXdd2g+P68Jf3e+NSKoZ/Z1MxCHxRxS8osHnduf3X08Um2aRgrD1LxVKBCkgMBiOodHu8EoqbTp4BRUUBIX68yWYqAGqM+nTyaqPhuKV3Pl/tl/MLjxsv51c/EkmMAmOhqdm0UZIMUHd/tMH3li0tHBbo1dJgtuvzh0tFADIAmE81rADyxbOTGX0+dQoo1LLulO6KwV0LT0Hs0lk2VaP0q2tmUxxngGIG9FOkMBHLdbKwPweKuuRRg/Sw0rSnoKJGVcFu5XkKRDGAjEoVbsLKidOBCnp4T1mNFvGgnIEi/VMOdTWq9ObayQNAqiPYVibmbAqCz6GoQEGjU8xkVFyidO0POKCHdZFTU9N2mr7n6OQKbEvJ4+1GBr7kUJ0ajapYWCxD30tlTF3oQ3NTCn0eoKGR0IuqsykihdgaQ30Hd0clem6w4Kb0sFSGHqMCsNO1nJkajRZ7qNwmHmiO2BkyKmNwjp52WzzX+SdwVxG5AR4CydkUfhVVUKTBo4FaLuYl2hkLGYQedo4cFH4yP2jsdrGqp0SVCjxZl05nQ4mqaIrpwHRd3ezpM0sX3jU3OYwiGTvUD3FXUxwHXUQdcN1ZHm8iu6FEonVx0WBZUKqm1AVf8aQQjqHdTbDNDA4PxLHAjNhxwtlSJb1reaJNFjmfgJ2iOHU3pbvHqoLnV1HAJydT1ol1mGsme6eO3F3JgB4x2J5FKp05ZS5u9jTSYFRg3AXLpj8KWTAIVkyr7abwlAJGjE65aZHMrwo3K5qjbCM5em+Zr3waKUPOR8P/iTX5cU/DH5CCdhCbVRqUiIELf+wgjz7NWQbaTfXIoGXoONZROyoKZKWMOVNtTpzhZ4mvfJ5sD6jYAjxOJcsvYE+rqQqUnxGitKcrVJ2C4nhxoQxAtilIDQk0dlNmJFXJIXCmFqdlFJl4eMD8I8BKTs9Ud00VBH3QByhdoExgZzr4CorIkxEmPY3cCyXhvAG1KvOdj3N91M47VlNo1ipxkCnS/I+2Vc6g2WB+nrlyLTyPup/HLeoVhZwLjZzV0zBwSEQiCiHk5BVcUqGAmV9oRSKDRUfXagp6OTxej5kVkU7yCZU4n+xsC8jGoq98llJDJ1ZghXkrutGcJ0UVq6ftRLC8GuEaWCJtwGqqGHUPhJkGiUQw9732vDO+LiuePksd1qT5GToZbgqdnFThVm7AEnFP27p0ZzhwgGcqjQ1mU7hqyvRSthJAZ5ehmtPfgW0Bt7R4+jRxhUYOfFJHeLYKDhEFItzT9rv4zSX/qQDXCXNVQZUUpRlgPYO2r9BIXFGj8wnRnSBW+MqnmFklo8G5U6zZ01CiWsTh0ZGq8nTXPOUMnbOOpqxgbcZ9e3ZVYDHv/i6mO8nrVgm4AwO4doILFLpqEOxlVE81PUicGSSTwWRI+1ecGssvSeSti7d2U9WBACOpYrn1AETNt+rGfDJwWMDh69N7eDhvtTEYlMyOOrMSrM4vrAGrRbDxjxp1rel55sjVYsMQbC0GMFYPzJkrxXdD++d3GAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYISW/wMr5C6ZkfY6QAAAAABJRU5ErkJggg=="
                  }
                  alt="Classic Bootstrap 4 React Dashboard Theme"
                  className="img-fluid rounded-lg landing-img academic"
                />
              </Link>
              <h4>Intern</h4>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  </section>
);

const Dashboards = () => (
  <section className="py-6 bg-white">
    <Container>
      <Row>
        <Col md="12" className="mx-auto text-center">
          <Row>
            <Col lg="10" xl="9" className="mx-auto">
              <div className="mb-4">
                <h1 className="mb-3 text-primary">Technology used</h1>
                <p className="text-muted font-size-lg">
                  Nguyen Duc Manh - 17113155 - PM22.12- Đại học Kinh Doanh Và
                  Công Nghệ Hà Nội
                </p>
              </div>
            </Col>
          </Row>

          <Row>
            <Col md="6" lg="4" className="py-3">
              <div to="#" target="_blank" className="d-block mb-3 mx-1">
                <img
                  src={no1}
                  alt="Bootstrap 4 Dashboard Theme"
                  className="img-fluid rounded-lg landing-img dashboard-img"
                />
              </div>
              <h4>Java Script</h4>
            </Col>
            <Col md="6" lg="4" className="py-3">
              <div to="#" target="_blank" className="d-block mb-3 mx-1">
                <img
                  src={no2}
                  alt="Analytics Bootstrap 4 Dashboard Theme"
                  className="img-fluid rounded-lg landing-img dashboard-img"
                />
              </div>
              <h4>My SQL</h4>
            </Col>
            <Col md="6" lg="4" className="py-3">
              <div to="#" target="_blank" className="d-block mb-3 mx-1">
                <img
                  src={no3}
                  alt="E-commerce Bootstrap 4 Dashboard Theme"
                  className="img-fluid rounded-lg landing-img dashboard-img"
                />
              </div>
              <h4>Spring Boot</h4>
            </Col>
            <Col md="6" lg="4" className="py-3">
              <div
                to="/dashboard/social"
                target="_blank"
                className="d-block mb-3 mx-1"
              >
                <img
                  src={no4}
                  alt="Social Bootstrap 4 Dashboard Theme"
                  className="img-fluid rounded-lg landing-img dashboard-img"
                />
              </div>
              <h4>React-JS</h4>
            </Col>
            <Col md="6" lg="4" className="py-3">
              <div
                to="/dashboard/crypto"
                target="_blank"
                className="d-block mb-3 mx-1"
              >
                <img
                  src={no5}
                  alt="Crypto Bootstrap 4 Dashboard Theme"
                  className="img-fluid rounded-lg landing-img dashboard-img"
                />
              </div>
              <h4>
                React-Strap{" "}
                <sup>
                  <Badge color="primary" tag="small">
                    New
                  </Badge>
                </sup>
              </h4>
            </Col>
            <Col md="6" lg="4" className="py-3">
              <div
                to="/pages/projects"
                target="_blank"
                className="d-block mb-3 mx-1"
              >
                <img
                  src={no6}
                  alt="Projects Bootstrap 4 Dashboard Theme"
                  className="img-fluid rounded-lg landing-img dashboard-img"
                />
              </div>
              <h4>
                ForMik{" "}
                <sup>
                  <Badge color="primary" tag="small">
                    New
                  </Badge>
                </sup>
              </h4>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  </section>
);

const Features = () => (
  <section className="py-6">
    <Container>
      <Row>
        <Col md="10" className="mx-auto text-center">
          <div className="mb-5">
            <h2>Features you'll love</h2>
            <p className="text-muted text-lg">
              A responsive dashboard built for everyone who wants to create
              webapps on top of Bootstrap.
            </p>
          </div>

          <Row className="text-left">
            <Col md="6">
              <Media className="py-3">
                <div className="landing-feature">
                  <Sliders />
                </div>
                <Media body>
                  <h4 className="mt-0">Customizable</h4>
                  <p className="font-size-lg">
                    You don't need to be an expert to customize our themes. Our
                    code is very readable and well documented.
                  </p>
                </Media>
              </Media>
            </Col>
            <Col md="6">
              <Media className="py-3">
                <div className="landing-feature">
                  <Smartphone />
                </div>
                <Media body>
                  <h4 className="mt-0">Fully Responsive</h4>
                  <p className="font-size-lg">
                    With mobile, tablet & desktop support it doesn't matter what
                    device you're using. AdminKit is responsive in all browsers.
                  </p>
                </Media>
              </Media>
            </Col>
            <Col md="6">
              <Media className="py-3">
                <div className="landing-feature">
                  <Mail />
                </div>
                <Media body>
                  <h4 className="mt-0">Dev-to-dev Support</h4>
                  <p className="font-size-lg">
                    Our themes are supported by specialists who provide quick
                    and effective support. Usually an email reply takes &lt;24h.
                  </p>
                </Media>
              </Media>
            </Col>
            <Col md="6">
              <Media className="py-3">
                <div className="landing-feature">
                  <Chrome />
                </div>
                <Media body>
                  <h4 className="mt-0">Cross Browser</h4>
                  <p className="font-size-lg">
                    Our themes are working perfectly with: Chrome, Firefox,
                    Safari, Opera and IE 10+. We're working hard to support
                    them.
                  </p>
                </Media>
              </Media>
            </Col>
            <Col md="6">
              <Media className="py-3">
                <div className="landing-feature">
                  <Code />
                </div>
                <Media body>
                  <h4 className="mt-0">Clean Code</h4>
                  <p className="font-size-lg">
                    We strictly follow Bootstrap's guidelines to make your
                    integration as easy as possible. All code is handwritten.
                  </p>
                </Media>
              </Media>
            </Col>
            <Col md="6">
              <Media className="py-3">
                <div className="landing-feature">
                  <i data-feather="download-cloud"></i>
                  <DownloadCloud />
                </div>
                <Media body>
                  <h4 className="mt-0">Regular Updates</h4>
                  <p className="font-size-lg">
                    From time to time you'll receive an update containing new
                    components, improvements and bugfixes.
                  </p>
                </Media>
              </Media>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  </section>
);

const Testimonials = () => (
  <section className="py-6 bg-white">
    <Container>
      <div className="mb-5 text-center">
        <h2>Developers love AppStack</h2>
        <p className="text-muted text-lg">
          Here's what some of our 3,000 customers have to say about working with
          our products.
        </p>
      </div>

      <Row>
        <Col md={6} lg={4}>
          <Card tag="blockquote" className="landing-quote border">
            <CardBody className="p-4">
              <div className="d-flex align-items-center mb-3">
                <div>
                  <img
                    src={brandBootstrapB}
                    width="48"
                    height="48"
                    alt="Bootstrap"
                  />
                </div>
                <div className="pl-3">
                  <h5 className="mb-1 mt-2">Nikita</h5>
                  <small className="d-block text-muted h5 font-weight-normal">
                    Bootstrap Themes
                  </small>
                </div>
              </div>
              <p className="lead mb-2">
                “We are totally amazed with a simplicity and the design of the
                template.{" "}
                <span>Probably saved us hundreds of hours of development.</span>{" "}
                We are absolutely amazed with the support Bootlab has provided
                us.”
              </p>

              <div className="landing-stars">
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col md={6} lg={4}>
          <Card tag="blockquote" className="landing-quote border">
            <CardBody className="p-4">
              <div className="d-flex align-items-center mb-3">
                <div>
                  <img
                    src={brandBootstrapB}
                    width="48"
                    height="48"
                    alt="Bootstrap"
                  />
                </div>
                <div className="pl-3">
                  <h5 className="mb-1 mt-2">Alejandro</h5>
                  <small className="d-block text-muted h5 font-weight-normal">
                    Bootstrap Themes
                  </small>
                </div>
              </div>
              <p className="lead mb-2">
                “Everything is so properly set up that{" "}
                <span>
                  any new additions I'd make would feel like a native extension
                  of the theme
                </span>{" "}
                versus a simple hack. I definitely feel like this will save me
                hundredths of hours I'd otherwise spend on designing.”
              </p>

              <div className="landing-stars">
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col md={6} lg={4}>
          <Card tag="blockquote" className="landing-quote border">
            <CardBody className="p-4">
              <div className="d-flex align-items-center mb-3">
                <div>
                  <img
                    src={brandBootstrapB}
                    width="48"
                    height="48"
                    alt="Bootstrap"
                  />
                </div>
                <div className="pl-3">
                  <h5 className="mb-1 mt-2">Jordi</h5>
                  <small className="d-block text-muted h5 font-weight-normal">
                    Bootstrap Themes
                  </small>
                </div>
              </div>
              <p className="lead mb-2">
                “I ran into a problem and contacted support. Within 24 hours, I
                not only received a response but even an updated version that
                solved my problem and works like a charm.{" "}
                <span>Fantastic customer service!</span>”
              </p>

              <div className="landing-stars">
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col md={6} lg={4}>
          <Card tag="blockquote" className="landing-quote border">
            <CardBody className="p-4">
              <div className="d-flex align-items-center mb-3">
                <div>
                  <img
                    src={brandBootstrapB}
                    width="48"
                    height="48"
                    alt="Bootstrap"
                  />
                </div>
                <div className="pl-3">
                  <h5 className="mb-1 mt-2">Jason</h5>
                  <small className="d-block text-muted h5 font-weight-normal">
                    Bootstrap Themes
                  </small>
                </div>
              </div>
              <p className="lead mb-2">
                “As a DB guy, this template has made my life easy porting over
                an old website to a new responsive version. I can{" "}
                <span>focus more on the data and less on the layout.</span>”
              </p>

              <div className="landing-stars">
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col md={6} lg={4}>
          <Card tag="blockquote" className="landing-quote border">
            <CardBody className="p-4">
              <div className="d-flex align-items-center mb-3">
                <div>
                  <img
                    src={brandBootstrapB}
                    width="48"
                    height="48"
                    alt="Bootstrap"
                  />
                </div>
                <div className="pl-3">
                  <h5 className="mb-1 mt-2">Richard</h5>
                  <small className="d-block text-muted h5 font-weight-normal">
                    Bootstrap Themes
                  </small>
                </div>
              </div>
              <p className="lead mb-2">
                “This template was just what we were after;{" "}
                <span>
                  its modern, works perfectly and is visually beautiful
                </span>
                , we couldn't be happier. The support really is excellent, I
                look forward to working with these guys for a long time to
                come!”
              </p>

              <div className="landing-stars">
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col md={6} lg={4}>
          <Card tag="blockquote" className="landing-quote border">
            <CardBody className="p-4">
              <div className="d-flex align-items-center mb-3">
                <div>
                  <img
                    src={brandBootstrapB}
                    width="48"
                    height="48"
                    alt="Bootstrap"
                  />
                </div>
                <div className="pl-3">
                  <h5 className="mb-1 mt-2">Martin</h5>
                  <small className="d-block text-muted h5 font-weight-normal">
                    Bootstrap Themes
                  </small>
                </div>
              </div>
              <p className="lead mb-2">
                “I just began to test and use this theme and I can find that it
                cover my expectatives.{" "}
                <span>Good support from the developer.</span>”
              </p>

              <div className="landing-stars">
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />{" "}
                <FontAwesomeIcon icon={faStar} />
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  </section>
);

const FaqQuestion = ({ onClick, isOpen, question, answer }) => (
  <Card className="border mb-3">
    <CardHeader className="cursor-pointer" onClick={onClick}>
      <h6 className="mb-0">{question}</h6>
    </CardHeader>
    <Collapse isOpen={isOpen}>
      <CardBody className="py-3">{answer}</CardBody>
    </Collapse>
  </Card>
);

const Faq = () => {
  const [activeQuestion, setActiveQuestion] = useState(0);

  return (
    <section className="pt-6 pb-3">
      <Container>
        <div className="mb-5 text-center">
          <h2>Frequently Asked Questions</h2>
          <p className="text-muted font-size-lg">
            Here are some of the answers you might be looking for.
          </p>
        </div>

        <Row>
          <Col md={9} lg={8} className="mx-auto">
            <FaqQuestion
              onClick={() => setActiveQuestion(0)}
              isOpen={activeQuestion === 0}
              question="Is there any support included?"
              answer={
                <>
                  You have access to the Software's online support services via
                  email for six (6) months from the purchase date. Please
                  contact us at{" "}
                  <a href="mailto:info@bootlab.io">info@bootlab.io</a> for any
                  questions.
                </>
              }
            />
            <FaqQuestion
              onClick={() => setActiveQuestion(1)}
              isOpen={activeQuestion === 1}
              question="How do I get a receipt for my purchase?"
              answer={
                <>
                  You'll receive an email from Bootstrap themes once your
                  purchase is complete. More info can be found{" "}
                  <a
                    href="https://themes.zendesk.com/hc/en-us/articles/360000011052-How-do-I-get-a-receipt-for-my-purchase-"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    here
                  </a>
                  .
                </>
              }
            />
            <FaqQuestion
              onClick={() => setActiveQuestion(2)}
              isOpen={activeQuestion === 2}
              question="What methods of payments are supported?"
              answer={
                <>
                  You can purchase the themes on Bootstrap Themes via any major
                  credit/debit card (via Stripe) or with your Paypal account. We
                  don't support cryptocurrencies or invoicing at this time.
                </>
              }
            />
          </Col>
        </Row>
      </Container>
    </section>
  );
};

const Footer = () => (
  <section className="landing-footer pb-6">
    <svg
      className="landing-footer-shape"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 100 1440 220"
    >
      <path
        fill="#F7F9FC"
        fill-opacity="1"
        d="M0,128L1440,256L1440,0L0,0Z"
      ></path>
    </svg>
    <Container className="text-center landing-footer-container">
      <Row>
        <Col md="9" lg="8" xl="6" className="mx-auto">
          <h2 className="h1 text-white mb-3">
            Join over 3,000 developers who are already working with our products
          </h2>
          <Button
            color="light"
            size="lg"
            href="https://themes.getbootstrap.com/product/appstack-react-admin-dashboard-template/"
            target="_blank"
            rel="noopener noreferrer"
            className="mt-n1"
          >
            Submit
          </Button>
        </Col>
      </Row>
    </Container>
  </section>
);

const Landing = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(enableCorporateTheme());
  }, [dispatch]);

  return (
    <React.Fragment>
      <Navigation />
      <Intro />
      <Styles />
      <Dashboards />
      <Features />
      <Testimonials />
      <Faq />
      <Footer />
    </React.Fragment>
  );
};

export default connect()(Landing);
