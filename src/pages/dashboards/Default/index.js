import React from "react";
import {Container, UncontrolledCarousel} from "reactstrap";
import Header from "./Header";
const items = [
    {
        src : `https://vtiacademy.edu.vn/upload/images/banner-trang-chu(1).png`
    },
    {
        src: `https://vtiacademy.edu.vn/upload/images/z2490184142089-d9767851cfc3ad5a0baec22c6424ed44.jpg`
    },
    {
        src: `https://vtiacademy.edu.vn/upload/images/49333500-731022190624558-3899730244914905088-n.jpg`
    },
    {
        src: `https://vtiacademy.edu.vn/upload/images/z2488139024192-52cb7b3912442a138cddc3e8d3ab8c5c.jpg`
    }
]

const Default = () => (
    <Container>
        <Header/>
        <UncontrolledCarousel items={items}/>
    </Container>
);

export default Default;