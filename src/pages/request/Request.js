import React, { useEffect, useState } from "react";
import * as Icon from "react-feather";
import {
  Card,
  CardBody,
  Col,
  Container,
  Row,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
} from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { connect } from "react-redux";
import { selectRequest } from "../../redux/selectors/RequestSelector";
import { getListRequestsAction } from "../../redux/actions/ViewRequestAction";
import RequestApi from "../../api/RequestApi";
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from "yup";
import { Formik, FastField, Form } from "formik";
import { toastr } from "react-redux-toastr";
const tableColumns = [
  {
    dataField: "id",
    text: "RequestId",
    sort: true,
  },
  {
    dataField: "classId",
    text: "ClassId",
    sort: true,
  },
  {
    dataField: "status",
    text: "RequestStatus",
    sort: true,
  },
  {
    dataField: "userId",
    text: "MentorId",
    sort: true,
  },
  {
    dataField: "className",
    text: "ClassName",
    sort: true,
  },
];

// const tableData = [
//   {
//     requestId: "1",
//     classId: "1",
//     requestStatus: "-1",
//     mentorId: 1,
//     className: "Rocket01"
//   },
//   {
//     requestId: "2",
//     classId: "2",
//     requestStatus: "-1",
//     mentorId: 2,
//     className: "Rocket02"
//   },
//   {
//     requestId: "3",
//     classId: "3",
//     requestStatus: "-1",
//     mentorId: 3,
//     className: "Rocket03"
//   },
// ]
const Request = (props) => {
  const getListRequest = props.getListRequestsAction;
  const getAllRequest = async () => {
    const result = await RequestApi.getAll();
    const requests = result;
    console.log(requests);
    getListRequest(requests);
  };
  //call api
  useEffect(() => {
    getAllRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getListRequest]);
  // Create
  const [isOpenModal, setOpenModal] = useState(false);

  const showSuccessNotification = (title, message) => {
    const options = {
      timeOut: 2500,
      showCloseButton: false,
      progressBar: false,
      position: "top-right",
    };

    // show notification
    toastr.success(title, message, options);
  };

  return (
    <Container fluid className="p-0">
      <h1 className="h3 mb-3">View Request</h1>
      <Row>
        <Col>
          <Card>
            <Row>
              <Col lg="12">
                <div className="float-right pull-right">
                  <Icon.Filter size="34" className="align-middle mr-2" />
                  <Icon.RefreshCcw size="34" className="align-middle mr-2" />
                  <Icon.PlusCircle
                    size="34"
                    className="align-middle mr-2"
                    onClick={() => setOpenModal(true)}
                  />
                </div>
              </Col>
            </Row>
            <CardBody>
              <BootstrapTable
                keyField="name"
                data={props.requests}
                columns={tableColumns}
                bootstrap4
                striped
                hover
                bordered
                pagination={paginationFactory({
                  sizePerPage: 5,
                  sizePerPageList: [5, 10, 25, 50],
                })}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Modal isOpen={isOpenModal}>
        <Formik
          initialValues={{
            classId: "",
            userId: "",
            className: "",
          }}
          validationSchema={Yup.object({
            //Validate
            className: Yup.string()
              .required("Required")
              .max(30, "Must be between 6 to 30 characters")
              .min(6, "Must be between 6 to 30 characters"),

            classId: Yup.string().required("Required"),

            userId: Yup.string().required("Required"),
          })}
          onSubmit={async (values) => {
            try {
              // call api
              await RequestApi.create(
                values.classId,
                values.userId,
                values.className
              );
              setOpenModal(false);
              await getAllRequest();
              // show notification
              await showSuccessNotification(
                "Create Group",
                "Create Group Successfully!"
              );
              // getListRequest();
              // reload group page
              // refreshForm();
            } catch (error) {
              console.log(error);
              setOpenModal(false);
            }
          }}
          validateOnChange={false}
          validateOnBlur={false}
        >
          {({ isSubmitting }) => (
            <Form>
              {/* header */}
              <ModalHeader>Create Request</ModalHeader>

              {/* body */}
              <ModalBody className="m-3">
                {/* Firstname */}
                <Row style={{ alignItems: "center" }}>
                  <Col>Class ID:</Col>
                  <Col>
                    <FastField
                      //label="Group Name"
                      bsSize="lg"
                      type="text"
                      name="classId"
                      placeholder="Enter Class ID"
                      component={ReactstrapInput}
                    />
                  </Col>
                </Row>
                <Row style={{ alignItems: "center" }}>
                  <Col>UserID:</Col>
                  <Col>
                    <FastField
                      //label="Group Name"
                      bsSize="lg"
                      type="text"
                      name="userId"
                      placeholder="Enter User ID"
                      component={ReactstrapInput}
                    />
                  </Col>
                </Row>
                <Row style={{ alignItems: "center" }}>
                  <Col>Class Name:</Col>
                  <Col>
                    <FastField
                      //label="Group Name"
                      bsSize="lg"
                      type="text"
                      name="className"
                      placeholder="Enter Class Name"
                      component={ReactstrapInput}
                    />
                  </Col>
                </Row>
              </ModalBody>

              {/* footer */}
              <ModalFooter>
                {/* resend */}
                <Button
                  color="primary"
                  style={{ marginLeft: 10 }}
                  disabled={isSubmitting}
                  type="submit"
                >
                  Save
                </Button>

                {/* close button */}
                <Button color="primary" onClick={() => setOpenModal(false)}>
                  Cancel
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>
    </Container>
  );
};
// export default Request;

const mapGlobalStateToProps = (state) => {
  return {
    requests: selectRequest(state),
  };
};

export default connect(mapGlobalStateToProps, { getListRequestsAction })(
  Request
);
