import React, { useEffect } from "react";

import { Card, CardBody, Col, Container, Row } from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { connect } from "react-redux";
import { selectClass} from "../../redux/selectors/ClassSelector";
import { getListClassAction } from "../../redux/actions/ClassAction";
import ClassApi from "../../api/ClassApi";
// import ToolkitProvider, { Search } from   'react-bootstrap-table2-toolkit';

const tableColumns = [
  {
    dataField: "classID",
    text: "ClassID",
    sort: true,
  },
  {
    dataField: "className",
    text: "ClassName",
    sort: true,
  },
  {
    dataField: "startDate",
    text: "StartDate",
    sort: true,
  },
  {
    dataField: "endDate",
    text: "EndDate",
    sort: true,
  },
  {
    dataField: "description",
    text: "Description",
    sort: true,
  },
  {
    dataField: "status",
    text: "Status",
    sort: true,
  },
  {
    dataField: "totalStudent",
    text: "TotalStudent",
    sort: true,
  },
  {
    dataField: "teachingForm",
    text: "TeachingForm",
    sort: true,
  },
];

const Class = (props) => {
  const getListClass =  props.getListClassAction;
  // const size = props.size;

      useEffect(() => {
      const getAllClass = async () => {
        const result = await ClassApi.getAll();
        const clazz = result;
        // console.log(clazz.totalItems);
        // const totalSize = result.totalItems;
     
        getListClass(clazz);
      };
      getAllClass();
    }, [getListClass]);

    // const handleTableChange = async (type, { page, sizePerPage }) => {
    //   console.log(type);
    //   console.log(page);
    //   console.log(sizePerPage);
    
      // reget data
    //   const result = await ClassApi.getAll(page, sizePerPage);
    //   const clazz = result.classes;
    //   const totalSize = result.totalItems;
    
    //   getListClass(clazz, totalSize);
    // }

  return (
  <Container fluid className="p-0">
    <h1 className="h3 mb-3">Class Management</h1>
    <Row>
      <Col>
        <Card>
          <CardBody>
            <BootstrapTable
              keyField="name"
              data={props.clazz}
              columns={tableColumns}
              bootstrap4
              striped
              hover
              bordered
              // remote
              pagination={paginationFactory({
                sizePerPage: 5,
                sizePerPageList: [5, 10, 25, 50],
              })}
              // onTableChange={handleTableChange}
            />
          </CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
)};

const mapGlobalStateToProps = state => {
  return {
    clazz: selectClass(state),
    // page: selectPage(state),
    // size: selectSize(state),
    // totalSize: selectTotalSize(state)
  };
};

export default connect(mapGlobalStateToProps, { getListClassAction })(Class);

