import { createSelector } from "@reduxjs/toolkit";
// Selector
const classSelector = (state) => state.Class;

const selectClassSelector = createSelector(
   classSelector,
    state => state.clazz);

// const selectPageSelector = createSelector(
//       classSelector,
//     state => state.page);
// const selectSizeSelector = createSelector(
//         classSelector,
//     state => state.size);
// const selectTotalSizeSelector = createSelector(
//           classSelector,
//     state => state.totalSize);
// Funcition
export const selectClass = (state) => {
  return selectClassSelector(state);
};

// export const selectPage = (state) => {
//   return selectPageSelector(state);
// };

// export const selectSize = (state) => {
//   return selectSizeSelector(state);
// };

// export const selectTotalSize = (state) => {
//   return selectTotalSizeSelector(state);
// };
