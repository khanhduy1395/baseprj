import { createSelector } from "@reduxjs/toolkit";
// Selector
const requestSelector = (state) => state.Request;

const selectRequestsSelector = createSelector(
   requestSelector,
    state => state.requests);


// Funcition
export const selectRequest = (state) => {
  return selectRequestsSelector(state);
};





