import { createSelector } from "@reduxjs/toolkit";
// Selector
const profileSelector = (state) => state.Profile;

const selectProfileSelector = createSelector(
   profileSelector,
    state => state.profiles);

// Funcition
export const selectProfile = (state) => {
  return selectProfileSelector(state);
};
