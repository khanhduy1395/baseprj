import * as types from "../constants";

const initialState = {
  requests: []
};

export default function reducer(state = initialState, actions) {
  switch (actions.type) {
    case types.LIST_REQUEST:
      return {
        ...state,
        requests: actions.payload
        // totalSize: actions.payload.totalSize
      };
    default:
      return state;
  }
}
