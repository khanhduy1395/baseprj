import * as types from "../constants";

const initialState = {
  profiles: []
};

export default function reducer(state = initialState, actions) {
  switch (actions.type) {
    case types.LIST_PROFILE:
      return {
        ...state,
        profiles: actions.payload
        // totalSize: actions.payload.totalSize
      };
    default:
      return state;
  }
}
