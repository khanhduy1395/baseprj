import { combineReducers } from "redux";

import sidebar from "./sidebarReducers";
import layout from "./layoutReducer";
import theme from "./themeReducer";
import UserLoginInfo from "./UserLoginInfoReducers";
import Class from "./ClassReducers";
import { reducer as toastr } from "react-redux-toastr";
import Request from "./ViewReducers";
import Profile from "./ProfileReducers"
export default combineReducers({
  sidebar,
  layout,
  theme,
  toastr,
  UserLoginInfo,
  Class,
  Profile,
  Request
});
