import * as types from "../constants";

const initialState = {
  clazz: [],
  // page: 1,
  // size: 10,
  // totalSize: 50
};

export default function reducer(state = initialState, actions) {
  switch (actions.type) {
    case types.LIST_CLASS:
      return {
        ...state,
        clazz: actions.payload
        // totalSize: actions.payload.totalSize
      };
    default:
      return state;
  }
}
