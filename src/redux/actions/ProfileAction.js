import * as types from "../constants";

export function getProfileAction(profiles) {
  return {
    type: types.LIST_PROFILE,
    payload: profiles
  };
}
