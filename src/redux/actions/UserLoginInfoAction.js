import * as types from "../constants";

export function setUserLoginInfo(userName, email, firstName, lastName, role, status,phone,address,description,avatarUrl) {
    return {
        type: types.User_LOGIN_INFO,
        payload: {
            "userName": userName,
            "email": email,
            "firstName": firstName,
            "lastName": lastName,
            "role": role,
            "status": status,
            "phone": phone,
            "address": address,
            "avatarUrl": avatarUrl,
            "description":description
        }
};
}
export function setTokenInfo(token) {
    return {
        type: types.TOKEN_INFO,
        payload: token
    };
}