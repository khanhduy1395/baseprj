import * as types from "../constants";

export function getListRequestsAction(requests) {
  return {
    type: types.LIST_REQUEST,
    payload: requests
  };
}
