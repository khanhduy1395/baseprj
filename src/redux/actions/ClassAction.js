import * as types from "../constants";

export function getListClassAction(clazz) {
  return {
    type: types.LIST_CLASS,
    payload: clazz
  };
}
